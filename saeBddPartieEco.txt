Économie durable et numérique 

	1.Présentez les caractéristiques de l’entreprise dans laquelle vous travaillez et du marché sur lequel elle intervient.





	2.Lister les catégories de données que vous traitez en les classant selon les typologies vues en cours.





	3.A quelle étape du cycle de vie de ces données intervenez-vous ? Quels sont les acteurs économiques impliqués en amont et en aval de votre activité ?


	



	4.Résumez en deux ou trois phrases les raisons pour lesquelles votre travail sur ces données est important pour l’entreprise pour laquelle vous travaillez.






1) 	L’entreprise Carseus est une firme du secteur tertiaire proposant un service de location de voiture de luxe. Nous sommes une SARL. 
	Mais nous souhaitons au plus vite nous introduire en bourse pour avoir un apport de capital afin de s’expandre à l’internationnale. En effet, l'entreprise CARSEUS ne produit qu'en France, son siège social se situe au Mans. L'ensemble de nos activités est concentré là-bas, néanmoins nous possèdons quand même d'autres agences en France.
	Nous nous positionons en avale de la filière de production puisque nous fournissons un service aux près de nos clients. Bien-sûr, notre modèle commerciale est le B to C.
	
	Nous essayons de nous imposer dans un marché de niche, celui de la location de voiture de luxe. Un marché dominé par quelque acteurs avec SIXT notemment.
	Il faudra donc se faire une place dans ce marché à caractère oligopolistique.
	Un marché qui est aussi important et prometteur car selon un rapport de Absolute Reports en 2019 ce marché atteindra les 22,5 milliards de dollars en 2022 contre 10 milliards en 2016
	Malheureusement la moitié de la clientèle de ce marché se situe en amérique contre seulement 29% en Europe. Il est donc impératif de s'étendre au-delà de l'Atlantique.
	
	Le marché dans lequel notre firme se situe coche la caractérisque de transparence des marché, en effet, tous les acteurs peuvent connaître parfaitement toutes les caractéristique et cela à tout moment.
	De plus, la location de voiture de luxe, comme son nom l'indique est un marché de luxe et est donc peu asujetti aux facteurs externes. Ce marché possède bien d'autres avantages venant du simple fiat qu'il s'inscrit dans "le luxe", notamment la perpetuel évolution, le rayonnement à l'international se fait plus facilement.


2)	Les données que nous traitons sont les coordonnées géographique des clients(adresse, code postal, ville), le nom et prenom, des informations juridiques(numéro de permis, date d'obtention du permis) et d'autres informations personnelles comme le numéro de téléphone. Concernant les fournisseurs, Carseus possède uniquement la nature du contrat que l'on a avec eux et quelle voiture nous fournissent-ils.	
	Pour la tyopologie de ces données:
	La nature juridique des data:
	Effectivement les data telles que le nom et prénom ou permis de conduire permet d'identifier la personne. En ce qui concerne  le cas des coordonnées géographiques, ces data restent rattachées à une personne identifiée(par les autres data). La nature juridique de nos données est donc personnelle, pour les raisons précédemment énoncées.
	Pour la méthode de collecte, les acteurs économiques concernés nous fournissent directemment les données, ces dernières sont indispensables à la consommation de nos services.
	Nos données sont structurées dans une base de donnée, elles y sont triées et classées par groupe, plus précisément par clients ou par fournisseurs, cela permet de faciliter le traitement et l'analyse de nos data.
	Enfin, nos données sont non-rivales, la consommation de celle-ci n'empêche un autre acteur de la consommer au même moment. De plus, nous traitons des données comme l'identité de nos clients, nous prenons donc les dispositions nécessaires pour qu'ils restent maître de ces données personnelles.
	De ce fait, nous déduisons que ces données sont excluables. On peut donc conclure que ces dernières sont des biens de club.
	


3)	Carseus récolte les données partagées directemment par les consommateurs, ensuite nous agissons à l'étape "d'analyse/utilisation" de la donnée afin de produire notre service, bien-sûr nous archivons ces données étant donné que nous avons surtout une clientèle d'habitués.
	Nous agissons également sur l'ensemble du cycle de vie de la data que nos créons avec nos fournisseurs(contrat/type de contrat), effectivement nous créons cette donnée, nous la traitons, puis la stock dans notre base de donnée, la partage pour assurer la valeur de transparence de notre firme.
	Etant donné que notre firme produit un service à partir d'un produit fini nous nous situons à l'avale du processus de production. Les seuls acteurs économiques présents en avale de notre activité sont les ménages et les entreprises de biens et services consommateurs de nos services.
	Concernant l'amont, nous pouvons retrouver essentiellement les entreprises de biens de production du secteur primaire récoltant les matières prmières nécessaires à la fabrication des voitures que nous utilisons, d'autres entreprises de bien de production qui elles, construisent des parties de le voiture et enfin les entreprises de biens de consommation chez qui nous nous fournissons les véhicules. 

4)	Johnny : Notre travail sur les données est très important pour l’entreprise car ce travail nous permet de référencer les clients qui louent nos voitures et permet donc d’avoir un suivi de nos activités. 
	Il peut nous permettre par exemple de faire des statistiques (Quel type de voiture est le plus loué ?...). 
	De plus, imaginons qu’un problème ait été signalé à telle date avec telle voiture, on peut retrouver le client qui l’avait loué à ce moment avec une simple requête ce qui peut offrir un gain de temps considérable pour des situations comme cela requérant des informations sur des clients ou des locations. 

	Alexis : L’utilisation de base de données dans l’entreprise que nous avons créée est primordiale, puisque celle-ci nous permet de répertorier tous nos clients, voiture que nous louons, les réservations etc... 
	Sans cette base de données trié correctement nous ne pourrions pas accéder facilement à toutes les informations que nous souhaitons, c’est pour cela que des identifiants uniques pour chaque data a été créer.  

	Maël : Ce travail fournit sur les données est fondamentale à notre activité, sans celui-ci nous ne pourrions efficacement produire notre service. En effet, nous ne pourrions louer nos véhicules efficacement ne sachant pas quand sont-ils disponibles.
	De plus, ce travail peut permettre de produire des renseignements économiques sur notre activité témoignants de la situation dans laquelle nous nous retrouvons, cela autorise à conjecturer les futurs possibilités qui s'offriont à nous et d'assurer notre pérénité. 