DROP TABLE Voiture, Type, Client, Réservation, Fournisseur;

CREATE TABLE Voiture(
	immatriculation CHAR(9) PRIMARY KEY CONSTRAINT Problème_Immatriculation CHECK(immatriculation ~ '[A-Z][A-Z]-[0-9][0-9][0-9]-[A-Z][A-Z]'),
	marque VARCHAR(50) NOT NULL,
	kilométrage NUMERIC(6) NOT NULL,
	couleur VARCHAR(40) NOT NULL,
	modèle VARCHAR(50) NOT NULL REFERENCES Type,
	nom_fournisseur VARCHAR(100) REFERENCES Fournisseur,
	année NUMERIC(4) NOT NULL CHECK(année > 1900 AND DATE_PART('year', current_date) - année >= 0)
);


CREATE TABLE Type(
	modèle VARCHAR(50) PRIMARY KEY,
	caution NUMERIC(6) NOT NULL
);

CREATE TABLE Client(
	num_permis CHAR(15) PRIMARY KEY,
	nom VARCHAR(40) NOT NULL,
	prenom VARCHAR(40) NOT NULL,
	année_obtention NUMERIC(4) NOT NULL CONSTRAINT Problème_Date CHECK(année_obtention <= DATE_PART('year', current_date) - 3 AND année_obtention > 1942),
	adresse VARCHAR(100) NOT NULL,
	code_postal NUMERIC(5) NOT NULL CHECK(9999 < code_postal AND code_postal < 99999),
	ville VARCHAR(100) NOT NULL,
	tel CHAR(15) NOT NULL CONSTRAINT Problème_Téléphone CHECK(tel ~ '[06,07].[0-9][0-9].[0-9][0-9].[0-9][0-9].[0-9][0-9]'),
	type_Client VARCHAR(15) NOT NULL CONSTRAINT PbTypeClient CHECK(type_Client IN('Particulier', 'Entreprise'))
);


CREATE TABLE Réservation(
	num_reservation NUMERIC(12) PRIMARY KEY,
	immatriculation CHAR(9) REFERENCES Voiture CHECK(immatriculation ~ '[A-Z][A-Z]-[0-9][0-9][0-9]-[A-Z][A-Z]'),
	num_permis CHAR(15) REFERENCES Client,
	disponibilité NUMERIC(1) NOT NULL CHECK(disponibilité = 0 OR disponibilité = 1),
	DateReservation DATE NOT NULL,
	DateRetour DATE CONSTRAINT Date_Retour CHECK (dateretour > DateReservation AND dateRetour = DateReservation + Durée * INTERVAL '1 day'),
	Durée NUMERIC(2) CONSTRAINT Problème_Durée CHECK(Durée <= 7 AND Durée >= 1),
	forfait NUMERIC(5,2) CONSTRAINT Problème_Forfait CHECK (Forfait > 0.00)
);


CREATE TABLE Fournisseur(
	nom_fournisseur VARCHAR(100) PRIMARY KEY,
	type_contrat VARCHAR(100) NOT NULL,
	date_contrat DATE NOT NULL
);


INSERT INTO Voiture VALUES('GR-056-ON','Bugatti',23189,'Noir','Chiron','Tutur2Luxe',2018);
INSERT INTO Voiture VALUES('EZ-667-EK','Koenigsegg',34129,'Jaune','Regera','Tutur2Luxe',2016);
INSERT INTO Voiture VALUES('PO-726-TG','Ferrari',65023,'Rouge','458 Italia','Formili1',2011);
INSERT INTO Voiture VALUES('CO-138-OL','Ford',10298,'Argent','Mustang GT','Ford',1969);
INSERT INTO Voiture VALUES('BI-132-NO','Bugatti',85543,'Noir et Rouge','Veyron','Roux2Scoure',2009);
INSERT INTO Voiture VALUES('VI-404-MS','Tesla',10298,'Argent','Cybertruck','Clapette',2022);
INSERT INTO Voiture VALUES('PQ-100-DB','Lamborghini',8765,'Vert','Aventador SV','LaQimatra',2022);

INSERT INTO Type VALUES('Veyron',150000);
INSERT INTO Type VALUES('Chiron',240000);
INSERT INTO Type VALUES('Regera',230000);
INSERT INTO Type VALUES('458 Italia',30000);
INSERT INTO Type VALUES('Cybertruck',9500);
INSERT INTO Type VALUES('Mustang GT',12300);
INSERT INTO Type VALUES('Aventador SV',35000);

INSERT INTO Client VALUES('654684321685486','Musk','Elon',1990,'3 Rue des Potiers',75000,'Paris', '06 18 16 69 48','Entreprise');
INSERT INTO Client VALUES('546513546843546','Natendo','Stéphane',1976,'21 Rue du Pape',21000,'Tisoumits','06 85 96 12 04','Entreprise');
INSERT INTO Client VALUES('487465443154873','Joseph','Hugue',2003,'91 Avenue de l’Union Soviétique',17742,'Mouscou','07 39 14 45 75','Particulier');
INSERT INTO Client VALUES('464384543544456','Cénou','John',1999,'47 Rue du Combattant',93000,'Ring','07 83 53 73 76','Particulier');
INSERT INTO Client VALUES('248798745615644','Delacroix','Joé',2012,'18 rue de Porte de la Chapelle',89623,'Saint-Augustin','06 65 87 64 45','Particulier');
INSERT INTO Client VALUES('454845115464846','Du-Bourg-Palette','Sacha',2010,'30 Boulevard de Lavanville',10100,'Kanto','06 78 69 41 65','Entreprise');
INSERT INTO Client VALUES('546843544654681','Galaire','Geoffroy',1986,'7 Avenue de Fleury Mérogis',91700,'Bouchaire-sur-mer', '06 46 38 12 32','Particulier');
INSERT INTO Client VALUES('787543487343547','Sangle','Glocou',2008,'6 Boulevard des Norutau',86853,'Papou','06 85 41 25 73','Entreprise');
INSERT INTO Client VALUES('257272727376578','Félicité','Alexis',1993,'58 Rue Montalembert',63000,'Clermont-Ferrand','06 13 46 38 68','Particulier');
INSERT INTO Client VALUES('284654984654389','Hirrite','Sam',2007,'147 Avenue Léon Blum',63000,'Clermont-Ferrand','07 35 46 98 75','Particulier');
INSERT INTO Client VALUES('543484345484354','Chiling Ben','Ben',1998,'18 Rue de Waterburries',41649,'Kentucky','06 23 60 87 01','Entreprise');
INSERT INTO Client VALUES('126868621651645','Mau','Enzo',2010,'62 Rue des Archives',75004,'Les Marais','06 69 96 48 85','Particulier');

INSERT INTO Fournisseur VALUES('Midas','Location','15/08/2022');
INSERT INTO Fournisseur VALUES('Clapette','Achat','14/02/2022');
INSERT INTO Fournisseur VALUES('Tutur2Luxe','Achat','12/09/2022');
INSERT INTO Fournisseur VALUES('Ford','Essai','09/11/2022');
INSERT INTO Fournisseur VALUES('LaQimatra','Location','01/04/2022');
INSERT INTO Fournisseur VALUES('Roux2Scoure','Achat','06/06/2022');
INSERT INTO Fournisseur VALUES('Formili1','Achat','03/03/2022');

INSERT INTO Réservation VALUES('168493756259','PO-726-TG','787543487343547',0,'28/09/2022','01/10/2022',3,465.81);
INSERT INTO Réservation VALUES('563248946321','VI-404-MS','546513546843546',0,'12/05/2022','13/05/2022',1,113.61);
INSERT INTO Réservation VALUES('468723496457','BI-132-NO','543484345484354',0,'5/12/2022','12/12/2022',7,927.12);
INSERT INTO Réservation VALUES('282758252532','PO-726-TG','248798745615644',0,'27/07/2022','29/07/2022',2,276.73);
INSERT INTO Réservation VALUES('587287914563','PQ-100-DB','454845115464846',0,'2/04/2022','8/04/2022',6,784.20);
INSERT INTO Réservation VALUES('987526341287','PO-726-TG','257272727376578',0,'27/08/2022','1/09/2022',5,693.35);
INSERT INTO Réservation VALUES('292758252532','VI-404-MS','284654984654389',0,'18/03/2022','23/03/2022',5,471.09);
INSERT INTO Réservation VALUES('462597585579','CO-138-OL','546843544654681',0,'11/11/2022','13/11/2022',2,228.40);
INSERT INTO Réservation VALUES('474517621275','CO-138-OL','487465443154873',0,'30/11/2022','6/12/2022',6,815.63);
INSERT INTO Réservation VALUES('754798782598','GR-056-ON','546513546843546',0,'13/05/2022','14/05/2022',1,142.47);
INSERT INTO Réservation VALUES('267687528757','VI-404-MS','654684321685486',0,'03/07/2022','5/07/2022',2,259.49);
INSERT INTO Réservation VALUES('785872778678','GR-056-ON','464384543544456',0,'15/02/2022','20/02/2022',5,803.75);
INSERT INTO Réservation VALUES('525287258724','BI-132-NO','464384543544456',0,'27/07/2022','30/07/2022',3,290.53);
INSERT INTO Réservation VALUES('282758252867','PO-726-TG','454845115464846',0,'07/03/2022','11/03/2022',4,354.25);
INSERT INTO Réservation VALUES('257695794285','BI-132-NO','543484345484354',0,'09/05/2022','16/05/2022',7,998.99);
INSERT INTO Réservation VALUES('229883782794','VI-404-MS','257272727376578',0,'02/04/2022','03/04/2022',1,099.27);

INSERT INTO Réservation VALUES('419411149497','PQ-100-DB','248798745615644',0,'24/12/2022','26/12/2022',2,359.24);
INSERT INTO Réservation VALUES('484651178468','PO-726-TG','546843544654681',0,'31/12/2022','1/01/2023',1,102.69);
INSERT INTO Réservation VALUES('978615615984','CO-138-OL','546513546843546',0,'03/03/2023','10/03/2023',7,964.25);
INSERT INTO Réservation VALUES('257695794285','CO-138-OL','543484345484354',0,'19/12/2022','23/12/2022',4,387.18);
INSERT INTO Réservation VALUES('971269215996','VI-404-MS','126868621651645',0,'12/02/2023','14/03/2023',2,217.89);
INSERT INTO Réservation VALUES('945816374982','BI-132-NO','454845115464846',0,'17/01/2023','23/01/2023',6,704.18);
INSERT INTO Réservation VALUES('978615615984','PQ-100-DB','464384543544456',0,'17/04/2023','20/04/2023',3,248.67);
INSERT INTO Réservation VALUES('858852278728','PO-726-TG','543484345484354',0,'09/03/2023','10/03/2023',1,076.43);
INSERT INTO Réservation VALUES('528424598754','BI-132-NO','546843544654681',0,'27/06/2023','1/07/2023',4,964.25);


#Requêtes

#Question 1
SELECT immatriculation
FROM Voiture
WHERE immatriculation NOT IN (SELECT immatriculation FROM réservation);

#Question 2
SELECT * FROM Réservation
WHERE num_permis IN(SELECT num_permis FROM Client WHERE type_client = 'Entreprise');

#Question 3

#Question 4
SELECT SUM(r1.forfait) AS total_particuliers, SUM(r2.forfait) AS total_entreprises
FROM Réservation r1, Réservation r2
WHERE r1.num_permis IN(SELECT num_permis FROM Client WHERE type_client = 'Particulier')
AND r2.num_permis IN(SELECT num_permis FROM Client WHERE type_client = 'Entreprise');